package wuttisen.kanokpol.lab8;

import java.awt.geom.Ellipse2D;

public class MyBall extends Ellipse2D.Double {
	public final static int diameter = 30;

	public MyBall(int x, int y) {
		super(x, y, diameter, diameter);
	}
}