package wuttisen.kanokpol.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2 {
	public MyFrameV3(String text) {
		super(text);
	}

	protected void addComponents() {
		add(new MyCanvasV3());
	}

	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My FrameV3");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}