package wuttisen.kanokpol.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;

	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setBackground(Color.BLACK);
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.drawOval((WIDTH - 300) / 2, (HEIGHT - 300) / 2, 300, 300);
		g2d.fillOval((WIDTH - 120) / 2, (HEIGHT - 150) / 2, 30, 60);
		g2d.fillOval((WIDTH + 60) / 2, (HEIGHT - 150) / 2, 30, 60);
		g2d.fillRect((WIDTH - 95) / 2, (HEIGHT + 130) / 2, 100, 10);

	}
}