package wuttisen.kanokpol.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {

	public MyCanvasV4() {
		super();
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED };
		MyBrick brick = new MyBrick(0, (HEIGHT / 3) - 10);
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 7; j++) {
				g2d.setStroke(new BasicStroke(5));
				brick.x = i * 80;
				brick.y = ((HEIGHT / 3) - 10) - j * 20;
				g2d.setColor(color[j]);
				g2d.fill(brick);
				g2d.setColor(Color.BLACK);
				g2d.draw(brick);
			}
		}
		MyBall ball = new MyBall((WIDTH / 2) - 15, HEIGHT - 40);
		MyPedal pedal = new MyPedal((WIDTH / 2) - 50, HEIGHT - 10);
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
}
