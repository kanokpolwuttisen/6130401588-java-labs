package wuttisen.kanokpol.lab6;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {

	protected JPanel reviewPanel, centerPanel, underPanel;
	protected JLabel typeLabel, reviewLabel;
	protected JTextArea reviewTextArea;
	protected JScrollPane scrollPane;
	protected JComboBox comboType;
	String[] type = new String[] { "Phone", "Tablet", "Smart TV" };

	public MobileDeviceFormV2(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		reviewPanel = new JPanel();
		reviewPanel.setLayout(new BorderLayout());
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		typeLabel = new JLabel("Type:");
		comboType = new JComboBox<String>(type);
		comboType.setEditable(true);

		reviewLabel = new JLabel("Review: ");
		reviewTextArea = new JTextArea(3, 35);
		reviewTextArea.setLineWrap(true);
		reviewTextArea.setWrapStyleWord(true);
		reviewTextArea.setEditable(true);
		reviewTextArea.setText("Bigger than previous Note phones in every way,"
				+ " the Samsung Galaxy Note 9 has a larger 6.4-inch screen, heftier 4,000mAh battery,"
				+ " and a massive 1TB of storage option.");

		scrollPane = new JScrollPane(reviewTextArea);
		/*
		 * scrollPane.setVerticalScrollBarPolicy(
		 * JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		 */
		topPanel.add(typeLabel);
		topPanel.add(comboType);
		reviewPanel.add(reviewLabel, BorderLayout.NORTH);
		reviewPanel.add(scrollPane, BorderLayout.CENTER);
		centerPanel.add(reviewPanel, BorderLayout.CENTER);
		windowPanel.add(centerPanel, BorderLayout.CENTER);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV2 mobileDeviceForm2 = new MobileDeviceFormV2("Mobile Device Form V2");
		mobileDeviceForm2.addComponents();
		mobileDeviceForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
