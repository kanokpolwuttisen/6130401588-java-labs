package wuttisen.kanokpol.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {

	protected JMenuBar mb;
	protected JMenu fileMenu, configMenu;
	protected JMenu colorMenuItem, sizeMenuItem;
	protected JMenuItem newMenuItem, openMenuItem, saveMenuItem, exitMenuItem;
	protected JPanel featuresPanel;
	protected JLabel featuresLabel;
	protected JList featuresList;
	String[] featuresString = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };

	public MobileDeviceFormV3(String title) {
		super(title);

	}

	protected void addComponents() {
		super.addComponents();
		featuresPanel = new JPanel();

		featuresPanel.setLayout(new GridLayout(0, 2));
		featuresLabel = new JLabel("Features:");
		featuresList = new JList(featuresString);
		featuresPanel.add(featuresLabel);
		featuresPanel.add(featuresList);
		centerPanel.add(featuresPanel, BorderLayout.NORTH);
		centerPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
	}

	protected void addMenus() {
		mb = new JMenuBar();
		fileMenu = new JMenu("File");
		mb.add(fileMenu);
		configMenu = new JMenu("Config");
		mb.add(configMenu);
		newMenuItem = new JMenuItem("New");
		fileMenu.add(newMenuItem);
		openMenuItem = new JMenuItem("Open");
		fileMenu.add(openMenuItem);
		saveMenuItem = new JMenuItem("Save");
		fileMenu.add(saveMenuItem);
		exitMenuItem = new JMenuItem("Exit");
		fileMenu.add(exitMenuItem);
		colorMenuItem = new JMenu("Color");
		configMenu.add(colorMenuItem);
		sizeMenuItem = new JMenu("Size");
		configMenu.add(sizeMenuItem);
		this.setJMenuBar(mb);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceForm3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceForm3.addComponents();
		mobileDeviceForm3.addMenus();
		mobileDeviceForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
