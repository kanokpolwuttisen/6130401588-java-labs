package wuttisen.kanokpol.lab6;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV1 extends MySimpleWindow {

	protected JPanel choosePanel, topPanel;
	protected JLabel brandLabel, modelLabel, weightLabel, priceLabel, osLabel;
	protected JTextField brandnameTxtField, modelTxtField, weightTxtField, priceTxtField;
	protected JRadioButton android, ios;
	protected ButtonGroup choose;

	public MobileDeviceFormV1(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();

		choosePanel = new JPanel();
		topPanel = new JPanel();

		topPanel.setLayout(new GridLayout(0, 2));

		brandLabel = new JLabel("Brand Name:");
		modelLabel = new JLabel("Model Name:");
		weightLabel = new JLabel("Weight (kg.):");
		priceLabel = new JLabel("Price (Baht):");
		osLabel = new JLabel("Mobile OS:");

		brandnameTxtField = new JTextField(15);
		modelTxtField = new JTextField(15);
		weightTxtField = new JTextField(15);
		priceTxtField = new JTextField(15);

		choose = new ButtonGroup();
		android = new JRadioButton("Android");
		ios = new JRadioButton("iOS");
		choose.add(android);
		choose.add(ios);

		choosePanel.add(android);
		choosePanel.add(ios);

		topPanel.add(brandLabel);
		topPanel.add(brandnameTxtField);
		topPanel.add(modelLabel);
		topPanel.add(modelTxtField);
		topPanel.add(weightLabel);
		topPanel.add(weightTxtField);
		topPanel.add(priceLabel);
		topPanel.add(priceTxtField);
		topPanel.add(osLabel);
		topPanel.add(choosePanel);

		windowPanel.add(topPanel, BorderLayout.NORTH);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceFormV1.addComponents();
		mobileDeviceFormV1.setFrameFeatures();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}