package wuttisen.kanokpol.lab6;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {
	
	protected JButton CancelButton;
	protected JButton OKButton;
	protected JPanel buttonsPanel, windowPanel;
	
	public MySimpleWindow (String title) {
		super(title);
	}
	protected void  addComponents() {
		CancelButton = new JButton("Cancel");
		OKButton = new JButton("OK");
		buttonsPanel = new JPanel();
		
		buttonsPanel.add(CancelButton);
		buttonsPanel.add(OKButton);
		
		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(buttonsPanel, BorderLayout.SOUTH);
	}
	
	protected void setFrameFeatures() {
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		
	}
	
	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}