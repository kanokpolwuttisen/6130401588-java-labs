package wuttisen.kanokpol.lab3;

import java.util.*;

public class MatchingPenny {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			
			Random random = new Random();
			int n_random = random.nextInt(2); 
			System.out.print("Enter head or tail: ");
			String S1 = sc.nextLine().toLowerCase();	
			String h, t;
			
			
			if (S1.equals("exit") || S1.equals("tail") || S1.equals("head")) {
				if (S1.equals("exit")) {
					System.out.println("Good bye");
					System.exit(0);
				} else if (n_random == 0) {
					h = "head";
					System.out.println("You play " + S1);	
					System.out.println("Computer play " + h);
					if (S1.equals(h)) {
						System.out.println("You win.");
					} else {
						System.out.println("Computer wins.");
					}
				} else if (n_random == 1) {
					t = "tail";
					System.out.println("You play " + S1);	
					System.out.println("Computer play " + t);
					if (S1.equals(t)) {
						System.out.println("You win.");
					} else {
						System.out.println("Computer wins.");
					} 
				} 
			}
			else {
				System.err.println("Incorrect input. head or tail only");
			}
		}
	}

}