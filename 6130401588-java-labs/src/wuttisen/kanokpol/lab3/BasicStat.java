package wuttisen.kanokpol.lab3;

import java.util.Arrays;

public class BasicStat {
	
	static double num[];
	static int numInput;
	
	public static void acceptInput(String[] args) {
		numInput = Integer.parseInt(args[0]);
		int args_num = args.length-1;
		String howmany = Integer.toString(args_num);
		num = new double[args_num];
		if(args[0].equals(howmany)) {
			for(int i = 0; i<args_num; i++) {
				num[i] = Double.parseDouble(args[i+1]);
			} 
		} else {
			System.err.println("<BasicStat> <numNumber> <numbers>...");
		}
	}
	
	public static void displayStats() {
		Arrays.sort(num);
		double max_number = num[numInput-1];
		double min_number = num[0];
		System.out.println("Max is " + max_number + " Min is " + min_number);
		
		double sum = 0;
		for(int i=0; i<num.length; i++) {
			sum+=num[i];
		}
		double avg = sum/num.length;
		System.out.println("Average is " + avg);
		double sigma = 0;
		for(int count=0; count<num.length; count++) {
			sigma+=(num[count]-avg)*(num[count]-avg);
		}
		double Standard_deviation = Math.sqrt(sigma/num.length);
		System.out.println("Standard Deviation is " +Standard_deviation);
		
	}
	public static void main(String[] args) {
		acceptInput(args);
		displayStats();
	}
}