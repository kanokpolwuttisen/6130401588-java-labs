package wuttisen.kanokpol.lab3;

import java.util.*; 

public class TypingTest {
	
	public static final double STCerrent = System.currentTimeMillis();
	
	public static void Time_Second() {
		double AnswerCerrent = System.currentTimeMillis();
		double ans = (AnswerCerrent - STCerrent) / 1000;
		System.out.println("Your time is " + ans);
		
		if (ans <= 12) {
			System.out.println("You type faster than average person");
		} else {
			System.out.println("You type slower than average person");
		}
		System.exit(0);
	}

	public static void main(String[] args) {
		
		String[] colors = {"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET"};
		
		StringBuilder sb1 = new StringBuilder("");
		for (int i = 0; i < 8; i++) {
			String colors_random = (colors[new Random().nextInt(colors.length)]);
			sb1.append(colors_random);
			sb1.append(" ");
		}
		String sb1new = sb1.substring(0, sb1.length()-1);
		System.out.println(sb1new);
		
		while(true) {
			System.out.print("Type your answer: ");
			Scanner scanner = new Scanner(System.in);
			String answer = scanner.nextLine().toUpperCase();
			
			if (answer.equals(sb1new)) {
				Time_Second();
				scanner.close();
			} else {
				continue;
			}
			
		
		}
	}

}
