package wuttisen.kanokpol.lab5;

public class AndroidSmartWatch extends AndroidDevice {
	private String brandName;
	private String modelName;
	private int price;
	
	public AndroidSmartWatch(String brandName, String modelName, int price) {
		super();
		this.brandName = brandName;
		this.modelName = modelName;
		this.price = price;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate, and your step count");
	}
	
	@Override
	public String toString() {
		return "AndroidSmartWatch [Brand name:" + brandName + ", Model name: " + modelName + ", price:" + price + " Baht]";
	}

	public void displayTime() {
		System.out.println("Display time only using a digital format");
	}
	

}
