package wuttisen.kanokpol.lab5;

/**
 * MobileDevice is to model a mobile device object with
 * attributes modelName, os, price, and weight
 * @author kanokpol wuttisen
 * @version 1.0
 * @since 2019-02-26
 *
 */
public class MobileDevice {

		private String modelName;
		private String os;
		private int price, weight;

		public MobileDevice(String modelName, String os, int price, int weight) {
			this.modelName = modelName;
			this.os = os;
			this.price = price;
			this.weight = weight;
		}

		public String getModelName() {
			return modelName;
		}

		public void setModelName(String modelName) {
			this.modelName = modelName;
		}

		public String getOs() {
			return os;
		}

		public void setOs(String os) {
			this.os = os;
		}

		public int getPrice() {
			return price;
		}

		public void setPrice(int price) {
			this.price = price;
		}

		public int getWeight() {
			return weight;
		}

		public void setWeight(int weight) {
			this.weight = weight;
		}

		@Override
		public String toString() {
			return "MobileDevice [Model name:" + modelName + ", OS: " + os + ", Price: " + price + " Baht, Weight=" + weight
					+ " g]";
		}
	}
