package wuttisen.kanokpol.lab5;

public class SamsungDevice extends MobileDevice {

		private String ModelName;
		private int price, weight;
		private double androidVersion;

		public SamsungDevice(String modelName, int price, int weight, double androidVersion) {
			super(modelName, "Android", price, weight);
			this.ModelName = modelName;
			this.price = price;
			this.weight = weight;
			this.androidVersion = androidVersion;
		}

		public static String getBrand() {
			String brand = " Samsung";
			return brand;
		}

		public double getAndroidVersion() {
			return androidVersion;
		}

		public void setAndroidVersion(double androidVersion) {
			this.androidVersion = androidVersion;
		}

		public String getModelName() {
			return ModelName;
		}

		public void setModelName(String modelName) {
			ModelName = modelName;
		}

		public int getPrice() {
			return price;
		}

		public void setPrice(int price) {
			this.price = price;
		}

		public int getWeight() {
			return weight;
		}

		public void setWeight(int weight) {
			this.weight = weight;
		}

		@Override
		public String toString() {
			return "SamsungDevice [Model name:" + ModelName + ", OS:" + getOs() + ", Price:" + price + " Baht" + ", weight:"  +weight
					+ " g" + ", androidVersion:" + androidVersion + "]";
		}
		
		public void displayTime() {
			System.out.println("Display times in both using a digital format and using an analog watch");
		}
	}
