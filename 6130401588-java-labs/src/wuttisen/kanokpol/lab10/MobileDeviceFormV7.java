package wuttisen.kanokpol.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import wuttisen.kanokpol.lab7.MobileDeviceFormV6;

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener {

	public MobileDeviceFormV7(String title) {
		super(title);
	}

	private void handleCancelButton() {
		brandnameTxtField.setText(" ");
		modelTxtField.setText(" ");
		weightTxtField.setText(" ");
		priceTxtField.setText(" ");
		reviewTextArea.setText(" ");
	}

	private void handleOKButton() {
		if (ios.isSelected()) {
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + brandnameTxtField.getText() + ", Model Name: " + modelTxtField.getText()
							+ ", Weight: " + weightTxtField.getText() + ", Price: " + priceTxtField.getText() + "\n"
							+ " OS: " + ios.getText() + "\n Type: " + comboType.getSelectedItem() + "\n Features: "
							+ featuresList.getSelectedValuesList() + "\n Review: " + reviewTextArea.getText());
		} else if (android.isSelected()) {
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + brandnameTxtField.getText() + ", Model Name: " + modelTxtField.getText()
							+ ", Weight: " + weightTxtField.getText() + ", Price: " + priceTxtField.getText() + "\n"
							+ " OS: " + android.getText() + "\n Type: " + comboType.getSelectedItem() + "\n Features: "
							+ featuresList.getSelectedValuesList() + "\n Review: " + reviewTextArea.getText());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object src = e.getSource();
		if (src == OKButton) {
			handleOKButton();
		} else if (src == CancelButton) {
			handleCancelButton();
		}
	}

	protected void addListeners() {
		OKButton.addActionListener(this);
		CancelButton.addActionListener(this);

		ios.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (ios.isSelected()) {
					JOptionPane.showMessageDialog(null, "Your os platform is now changed to iOS");
				}
			}
		});

		android.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (android.isSelected()) {
					JOptionPane.showMessageDialog(null, "Your os platform is now changed to Android OS");

				}
			}
		});

		featuresList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				if (event.getValueIsAdjusting())
					return;
				JOptionPane.showMessageDialog(null, featuresList.getSelectedValuesList());
			}
		});

		comboType.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (e.getStateChange() == ItemEvent.SELECTED) {
					JOptionPane.showMessageDialog(null, "Type is updated to " + comboType.getSelectedItem());
				}
			}
		});
	}

	protected void addComponents() {
		super.addComponents();
		featuresList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		featuresList.setSelectedIndex(3);
		featuresList.addSelectionInterval(0, 1);
		android.setSelected(true);
	}

	protected void addMenus() {
		super.addMenus();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV7 MobileDeviceForm7 = new MobileDeviceFormV7("Mobile Device Form V7");
		MobileDeviceForm7.addComponents();
		MobileDeviceForm7.addMenus();
		MobileDeviceForm7.setFrameFeatures();
		MobileDeviceForm7.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
