package wuttisen.kanokpol.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 implements ActionListener {
	protected JMenuItem customItem;

	public MobileDeviceFormV8(String title) {
		super(title);
	}

	protected void addCustomMenu() {
		customItem = new JMenuItem("Custom...");
		colorMenuItem.add(customItem);
	}

	protected void addMenus() {
		super.addMenus();
		addCustomMenu();
		addMnemonic();
		addAccelerator();
	}

	protected void addMnemonic() {
		fileMenu.setMnemonic(KeyEvent.VK_F);
		newMenuItem.setMnemonic(KeyEvent.VK_N);
		openMenuItem.setMnemonic(KeyEvent.VK_O);
		saveMenuItem.setMnemonic(KeyEvent.VK_S);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		configMenu.setMnemonic(KeyEvent.VK_C);
		colorMenuItem.setMnemonic(KeyEvent.VK_L);
		blueMI.setMnemonic(KeyEvent.VK_B);
		greenMI.setMnemonic(KeyEvent.VK_G);
		redMI.setMnemonic(KeyEvent.VK_R);
		customItem.setMnemonic(KeyEvent.VK_U);
	}

	public void addAccelerator() {
		newMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		blueMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		greenMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		redMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		customItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
	}

	protected void addComponents() {
		super.addComponents();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV8 mobileDeviceForm8 = new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceForm8.addComponents();
		mobileDeviceForm8.addMenus();
		mobileDeviceForm8.getPreferredSize();
		mobileDeviceForm8.setFrameFeatures();
		mobileDeviceForm8.addListeners();

	}
}
