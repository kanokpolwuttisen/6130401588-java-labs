package wuttisen.kanokpol.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import wuttisen.kanokpol.lab8.MyBall;
import wuttisen.kanokpol.lab8.MyCanvas;

public class MyCanvasV6 extends MyCanvasV5 
	implements Runnable {
	protected MyBallV2 ball2 = new MyBallV2(MyCanvas.WIDTH/2 - MyBall.diameter/2, MyCanvas.HEIGHT/2 - MyBall.diameter/2);
	protected Thread running2 = new Thread(this);
	
	public MyCanvasV6() {
		super();
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running2.start();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0,0,super.WIDTH,super.HEIGHT));
		//Draw a ball
		g2d.setColor(Color.WHITE);
		g2d.fill(ball2);
	}
	
	@Override
	public void run() {
		while(true) {
			//bounce the ball when hit the right or the left side of the wall
			if(ball2.x <= 0 || ball2.x + MyBall.diameter >= MyCanvas.WIDTH) {
				ball2.ballVelX *= -1;
			}
			//bouncing the ball when it hits the top and bottom of the wall 
			if(ball2.y <= 0 || ball2.y + MyBall.diameter >= MyCanvas.HEIGHT) {
				ball2.ballVelY *= -1;
			}
			//move the ball
			ball2.move();
			repaint();
			//Delay
			try {
				Thread.sleep(10);
			}
			catch(InterruptedException ex) {
	
			}
		}
	}
}