package wuttisen.kanokpol.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV6 extends MyFrameV5 {
	public MyFrameV6(String text) {
		super(text);
	}

	protected void addComponents() {
		add(new MyCanvasV6());
	}

	public static void createAndShowGUI() {
		MyFrameV6 msw = new MyFrameV6("My Frame V6");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}