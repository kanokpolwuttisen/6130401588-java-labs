package wuttisen.kanokpol.lab9;

import javax.swing.SwingUtilities;

import wuttisen.kanokpol.lab8.MyFrameV4;

public class MyFrameV5 extends MyFrameV4 {
	public MyFrameV5(String text) {
		super(text);
	}

	protected void addComponents() {
		add(new MyCanvasV5());
	}

	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}