package wuttisen.kanokpol.lab9;

import wuttisen.kanokpol.lab8.MyBall;

public class MyBallV2 extends MyBall{
	
	protected int ballVelX;
	protected int ballVelY;
	
	public MyBallV2(int x, int y) {
		super(x, y);
	}
	
	public void move() {
		x += ballVelX;
		y += ballVelY;
	}
}