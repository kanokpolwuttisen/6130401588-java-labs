package wuttisen.kanokpol.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import wuttisen.kanokpol.lab8.MyBall;
import wuttisen.kanokpol.lab8.MyBrick;
import wuttisen.kanokpol.lab8.MyCanvas;

public class MyCanvasV7 extends MyCanvasV6 implements Runnable {
	protected Thread running = new Thread(this);
	protected int numBricks = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected MyBrickV2[] brick = new MyBrickV2[numBricks];

	public MyCanvasV7() {
		ball = new MyBallV2(0, 0);
		ball.ballVelX = 2;
		ball.ballVelY = 2;
		for (int i = 0; i < numBricks; i++) {
			brick[i] = new MyBrickV2(MyBrick.brickWidth * i, MyCanvasV7.HEIGHT / 2);
		}
		running.start();
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		// Draw a brick
		for (int i = 0; i < numBricks; i++) {
			g2d.setColor(Color.RED);
			g2d.fill(brick[i]);
			g2d.setColor(Color.BLUE);
			g2d.setStroke(new BasicStroke(4));
			g2d.draw(brick[i]);
			if (!brick[i].visible) {
				g2d.setColor(Color.BLACK);
				g2d.fill(brick[i]);
				g2d.setColor(Color.BLACK);
				g2d.setStroke(new BasicStroke(4));
				g2d.draw(brick[i]);
			}
		}
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}

	@Override
	public void run() {
		while (true) {
			// bounce off the right and left of the wall.
			if (ball.x + MyBall.diameter >= MyCanvas.WIDTH) {
				ball.x = MyCanvas.WIDTH - MyBall.diameter;
				ball.ballVelX *= -1;
			} else if (ball.x + MyBall.diameter <= 30) {
				ball.x = 0;
				ball.ballVelX *= -1;
			}
			// bouncing off the top and bottom
			else if (ball.y + MyBall.diameter >= MyCanvas.HEIGHT) {
				ball.y = MyCanvas.HEIGHT - MyBall.diameter;
				ball.ballVelY *= -1;
			} else if (ball.y + MyBall.diameter <= 30) {
				ball.y = 0;
				ball.ballVelY *= -1;
			}

			for (int i = 0; i < numBricks; i++) {
				if (brick[i].visible) {
					checkCollition(ball, brick[i]);
				}
			}
			// move the ball
			ball.move();
			repaint();
			// Delay
			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {

			}
		}
	}

	public void checkCollition(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) { // collides top or bottom
				// Change the direction on y coordinate to the opposite
				ball.ballVelY *= -1;
				
			} else { // collides left or right
				// Change the direction on x coordinate to the opposite
				ball.ballVelX *= -1;

			}
			ball.move();
			brick.visible = false;
		}
	}

}