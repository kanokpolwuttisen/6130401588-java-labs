package wuttisen.kanokpol.lab9;

import wuttisen.kanokpol.lab8.MyCanvas;
import wuttisen.kanokpol.lab8.MyPedal;

public class MyPedalV2 extends MyPedal{
	protected final static int speedPedal = 20;
	//declare speedPedal
	
	public MyPedalV2(int x, int y) {
		super(x, y);
	}
	
	//Add the code for moveLeft()
	public void moveLeft() {
		if(x-speedPedal <0) {
			x=0;
		} else {
			x-=speedPedal;
		}
	}
	
	//Add the code for moveRight()
	public void moveRight() {
		if(speedPedal + x > MyCanvas.WIDTH) {
			x = MyCanvas.WIDTH - MyPedal.pedalWidth;
		} else {
			x += speedPedal;
		}
	}
}
