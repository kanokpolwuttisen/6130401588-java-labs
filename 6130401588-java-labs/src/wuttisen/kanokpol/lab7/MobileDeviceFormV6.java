package wuttisen.kanokpol.lab7;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {

	protected JPanel imagesPanel;

	public MobileDeviceFormV6(String title) {
		super(title);
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		ReadImage imageShow = new ReadImage();

		imagesPanel = new JPanel();
		reviewPanel.add(imageShow, BorderLayout.SOUTH);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV6 mobileDeviceForm6 = new MobileDeviceFormV6("Mobile Device Form V6");
		mobileDeviceForm6.addComponents();
		mobileDeviceForm6.addMenus();
		mobileDeviceForm6.setFrameFeatures();
	}
}