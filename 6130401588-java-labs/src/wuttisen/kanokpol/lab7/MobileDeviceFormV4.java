package wuttisen.kanokpol.lab7;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;


import wuttisen.kanokpol.lab6.MobileDeviceFormV3;

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	protected JMenuItem blueMI,greenMI,redMI;
	public MobileDeviceFormV4(String title) {
		super(title);
		
	}
	
	protected void updateMenuIcon() {
		newMenuItem.setIcon(new ImageIcon("images/new.jpg"));
	}
	
	protected void addSubMenu() {
		redMI = new JMenuItem("Red");
		greenMI = new JMenuItem("Green");
		blueMI = new JMenuItem("Blue");
		colorMenuItem.add(redMI);
		colorMenuItem.add(greenMI);
		colorMenuItem.add(blueMI);
		sizeMenuItem.add(new JMenuItem("16"));
		sizeMenuItem.add(new JMenuItem("20"));
		sizeMenuItem.add(new JMenuItem("24"));
	}
	
	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenu();
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceForm4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceForm4.addComponents();
		mobileDeviceForm4.addMenus();
		mobileDeviceForm4.setFrameFeatures();
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
