package wuttisen.kanokpol.lab7;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

	public MobileDeviceFormV5(String title) {
		super(title);

	}

	@Override
	protected void addComponents() {
		super.addComponents();
		OKButton.setForeground(Color.BLUE);
		CancelButton.setForeground(Color.RED);
		brandLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		modelLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		weightLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		priceLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		osLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		typeLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		ios.setFont(new Font("Serif", Font.PLAIN, 14));
		android.setFont(new Font("Serif", Font.PLAIN, 14));
		reviewLabel.setFont(new Font("Serif", Font.PLAIN, 14));
		featuresLabel.setFont(new Font("Serif", Font.PLAIN, 14));

		brandnameTxtField.setFont(new Font("Serif", Font.BOLD, 14));
		modelTxtField.setFont(new Font("Serif", Font.BOLD, 14));
		weightTxtField.setFont(new Font("Serif", Font.BOLD, 14));
		priceTxtField.setFont(new Font("Serif", Font.BOLD, 14));
		reviewTextArea.setFont(new Font("Serif", Font.BOLD, 14));
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceForm5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceForm5.addComponents();
		mobileDeviceForm5.addMenus();
		mobileDeviceForm5.setFrameFeatures();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
